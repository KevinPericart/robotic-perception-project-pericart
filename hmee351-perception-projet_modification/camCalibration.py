#Author: andrea cherubini cherubini@lirmm.fr
#Script for calibrating e-puck camera given uv_xyz.dat file pairing the pixel 
#coordinates to their metric position in the calibration cube reference frame
#Input: the uv_xyz.dat file
#Output:  
#Options:

import numpy as np
import math as m
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.image as mpimg

#load file to a matrix 
#dataMat = np.loadtxt("uv_xyz (copie 1).dat")
dataMat = np.loadtxt("uv_xyz.dat")
u=dataMat[:,0]
v=dataMat[:,1]
x=dataMat[:,2]
y=dataMat[:,3]
z=dataMat[:,4]
nPixels=np.size(z);

#fill A and B matrices for least square estimation and solve Ax= b*
A=np.zeros((2*nPixels, 11))
b=np.zeros((2*nPixels, 1))


j=0;

for i in range(0, nPixels):
    A[j,:]=[x[i],y[i],z[i],1,0,0,0,0,-u[i]*x[i],-u[i]*y[i],-u[i]*z[i]];#TODO
    b[j,:]=u[i];#TODO
    
    A[j+1,:]=[0,0,0,0,x[i],y[i],z[i],1,-v[i]*x[i],-v[i]*y[i],-v[i]*z[i]];#TODO
    b[j+1,:]=v[i];#TODO
    j = j+2
print("A = ")
print(A)
print("B = ")
print(b)
X=np.linalg.pinv(A)@b;
Xt=np.concatenate((np.transpose(X),np.ones((1,1))), axis=1)




#design C and extract its three lines
C=np.concatenate((Xt[:,0:4], Xt[:,4:8],Xt[:,8:12]), axis=0)
print("C = ")
print(C)
c1=C[0,:3]
c2=C[1,:3]
c3=C[2,:3]



#compute scale factor lambda (noted L here)
L = np.linalg.norm(c3)
print("L = ")
print(L)





#compute intrinsic parameters

uo= (c1 @ c3)/(L**2) #TODO
vo= (c2 @ c3)/(L**2) #TODO
print("uo = ")
print(uo)
print("vo = ")
print(vo)
alpha_u= m.sqrt((c1 @ c1)/(L**2) - uo**2)#TODO
alpha_v= m.sqrt((c2 @ c2)/(L**2) - vo**2)#TODO
print("alpha_u = ")
print(alpha_u)
print("alpha_v = ")
print(alpha_v)



#compute extrinsic parameters
r1= (c1 - uo*c3) / (L*alpha_u) #TODO
r2= (c2 - vo*c3) / (L*alpha_v) #TODO
r1n=(r1/np.linalg.norm(r1)).reshape(1,3)
r2n=(r2/np.linalg.norm(r2)).reshape(1,3)
r3= (c3/L).reshape(1,3)#TODO
R=np.concatenate((r1n,r2n,r3), axis=0)
print("r1 = ",r1)
print("r2 = ",r2)
print("r3 = ",r3)
print("R = ",R)
px= (C[0,3] - uo*C[2,3])/(L*alpha_u) #TODO
py= (C[1,3] - vo*C[2,3])/(L*alpha_v)#TODO
pz= C[2,3]/L#TODO
p=np.array([[px],[py],[pz]])
print("p = ",p)

#      CpR = CpW - RpW
RpW = np.array([[0],[0.215],[0.025]])
CpR = p - RpW;
print("CpR = ",CpR)


#caluler u,v (U,V,div par S ?)
#boucle for puis calculer avec les cNN en fonction des x(i), y(i) et z(i)
#puis plot les points sur l'image 

UVS = np.zeros((3,12))
uv = np.zeros((3,12))
XYZ = np.ones((4,12))

for i in range(0, 12):
    XYZ[0,i] = x[i];
    XYZ[1,i] = y[i];
    XYZ[2,i] = z[i];



for i in range(0, 12):
    UVS[:,i] = L * (C @ XYZ[:,i]);
#1er l /3eme
uv[0,:] = UVS[0,:] / UVS[2,:]
uv[1,:] = UVS[1,:] / UVS[2,:] 
 
#im = Image.open("/home/pericartkevin/ros2_ws/src/hmee351-perception-projet/image0001.png");
#f = Image.open("image0001.png").show()


img = mpimg.imread('image0001.png')
plt.imshow(img)
plt.scatter(uv[0,:],uv[1,:], c='red')




# Load data from file
dataMat = np.loadtxt("uv_xyz2.dat")
u = dataMat[:, 0]
v = dataMat[:, 1]
x = dataMat[:, 2]
y = dataMat[:, 3]
z = dataMat[:, 4]
nPixels = np.size(z)

# Fill A and B matrices for least square estimation and solve Ax= b*
A = np.zeros((2 * nPixels, 11))
b = np.zeros((2 * nPixels, 1))

j = 0

for i in range(0, nPixels):
    A[j, :] = [x[i], y[i], z[i], 1, 0, 0, 0, 0, -u[i] * x[i], -u[i] * y[i], -u[i] * z[i]]
    b[j, :] = u[i]
    A[j + 1, :] = [0, 0, 0, 0, x[i], y[i], z[i], 1, -v[i] * x[i], -v[i] * y[i], -v[i] * z[i]]
    b[j + 1, :] = v[i]
    j = j + 2

# Solve for X
X = np.linalg.pinv(A) @ b
Xt = np.concatenate((np.transpose(X), np.ones((1, 1))), axis=1)

# Design C and extract its three lines
C = np.concatenate((Xt[:, 0:4], Xt[:, 4:8], Xt[:, 8:12]), axis=0)
c1 = C[0, :3]
c2 = C[1, :3]
c3 = C[2, :3]

# Compute scale factor lambda (noted L here)
L = np.linalg.norm(c3)

# Compute intrinsic parameters
uo = (c1 @ c3) / (L ** 2)
vo = (c2 @ c3) / (L ** 2)
alpha_u = m.sqrt((c1 @ c1) / (L ** 2) - uo ** 2)
alpha_v = m.sqrt((c2 @ c2) / (L ** 2) - vo ** 2)

# Compute extrinsic parameters
r1 = (c1 - uo * c3) / (L * alpha_u)
r2 = (c2 - vo * c3) / (L * alpha_v)
r1n = (r1 / np.linalg.norm(r1)).reshape(1, 3)
r2n = (r2 / np.linalg.norm(r2)).reshape(1, 3)
r3 = (c3 / L).reshape(1, 3)
R = np.concatenate((r1n, r2n, r3), axis=0)

px = (C[0, 3] - uo * C[2, 3]) / (L * alpha_u)
py = (C[1, 3] - vo * C[2, 3]) / (L * alpha_v)
pz = C[2, 3] / L
p = np.array([[px], [py], [pz]])

# Compute CpR
RpW = np.array([[0], [0.215], [0.025]])
CpR = p - RpW

# Calculate u,v (U,V,div par S ?)
UVS = np.zeros((3, nPixels))
uv2 = np.zeros((3, nPixels))
XYZ = np.ones((4, nPixels))

for i in range(0, nPixels):
    XYZ[0, i] = x[i]
    XYZ[1, i] = y[i]
    XYZ[2, i] = z[i]

for i in range(0, nPixels):
    UVS[:, i] = L * (C @ XYZ[:, i])

uv2[0, :] = UVS[0, :] / UVS[2, :]
uv2[1, :] = UVS[1, :] / UVS[2, :]

plt.scatter(uv2[0, :], uv2[1, :])

#img = mpimg.imread('image0001.png')
#plt.imshow(img)
plt.scatter(uv2[0, :], uv2[1, :], c='blue')
plt.show()


plt.show()
#fichier Tp epuck hae608




# Display camera and cube frames
if __name__ == '__main__':
    fig = plt.figure(figsize=(8, 8)) 
    ax1 = fig.add_subplot(111, projection='3d')
    ax1.set_xlabel('X')
    ax1.set_xlim(-0.4, 0.4)  
    ax1.set_ylabel('Y')
    ax1.set_ylim(-0.4, 0.4)  
    ax1.set_zlabel('Z')
    ax1.set_zlim(-0.4, 0.4)  

    # Camera frame in pyplot frame
    O_cam = (0, 0, 0)
    X_cam = (1, 0, 0)
    Y_cam = (0, 0, -1)
    Z_cam = (0, 1, 0)

    # Arrow properties
    arrow_prop_dict = dict(arrowstyle='->', mutation_scale=15, lw=2, color='r')

    # Add arrows manually
    ax1.quiver(*O_cam, *X_cam, length=0.5, normalize=True, color='r')
    ax1.quiver(*O_cam, *Y_cam, length=0.5, normalize=True, color='g')
    ax1.quiver(*O_cam, *Z_cam, length=0.5, normalize=True, color='b')

    ax1.text(-0.1, 0, 0, r'$C$')

    # Object (cube) frame (transformed wrt pyplot frame)
    R_pypCam = np.array([[1, 0, 0], [0, 0, 1], [0, -1, 0]])
    O_wor = R_pypCam @ p
    X_wor = R_pypCam @ (p + R[:, [0]])
    Y_wor = R_pypCam @ (p + R[:, [1]])
    Z_wor = R_pypCam @ (p + R[:, [2]])

    # Add arrows manually
    ax1.quiver(*O_wor[:, 0], *X_wor[:, 0], length=0.5, normalize=True, color='r')
    ax1.quiver(*O_wor[:, 0], *Y_wor[:, 0], length=0.5, normalize=True, color='g')
    ax1.quiver(*O_wor[:, 0], *Z_wor[:, 0], length=0.5, normalize=True, color='b')

    ax1.text(0, 0, 0.1, r'$W$')
    plt.show()










##display camera and cube frames
#import matplotlib.pyplot as plt
#from matplotlib.patches import FancyArrowPatch
#from mpl_toolkits.mplot3d import proj3d
#
#class Arrow3D(FancyArrowPatch):
#    def __init__(self, xs, ys, zs, *args, **kwargs):
#        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
#        self._verts3d = xs, ys, zs
#
#    def draw(self, renderer):
#        xs3d, ys3d, zs3d = self._verts3d
#        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
#        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
#        FancyArrowPatch.draw(self, renderer)
#
#if __name__ == '__main__':
#    fig = plt.figure()
#    ax1 = fig.add_subplot(111, projection='3d')
#    ax1.set_xlabel('X')
#    ax1.set_xlim(-.5, .5)
#    ax1.set_ylabel('Y')
#    ax1.set_ylim(0, 1)
#    ax1.set_zlabel('Z')
#    ax1.set_zlim(-.5, .5)
#
##camera frame in pyplot frame
#O_cam = (0, 0, 0)
#X_cam = (1, 0, 0)
#Y_cam = (0, 0, -1)
#Z_cam = (0, 1, 0)
#arrow_prop_dict = dict(mutation_scale=20, arrowstyle='->', shrinkA=0, shrinkB=0)
#a = Arrow3D([O_cam[0], X_cam[0]], [O_cam[1], X_cam[1]], [O_cam[2], X_cam[2]], **arrow_prop_dict, color='r')
#ax1.add_artist(a)
#a = Arrow3D([O_cam[0], Y_cam[0]], [O_cam[1], Y_cam[1]], [O_cam[2], Y_cam[2]], **arrow_prop_dict, color='g')
#ax1.add_artist(a)
#a = Arrow3D([O_cam[0], Z_cam[0]], [O_cam[1], Z_cam[1]], [O_cam[2], Z_cam[2]], **arrow_prop_dict, color='b')
#ax1.add_artist(a)
#ax1.text(-0.1, 0, 0, r'$C$')
#
##object (cube) frame (transformed wrt pyplot frame)
#R_pypCam = np.array([[1, 0, 0],[0, 0, 1],[0, -1, 0]])
#O_wor = R_pypCam @ p;
#X_wor = R_pypCam @ (p + R[:,[0]]);
#Y_wor = R_pypCam @ (p + R[:,[1]]);
#Z_wor = R_pypCam @ (p + R[:,[2]]);
#a = Arrow3D([O_wor[0][0], X_wor[0][0]], [O_wor[1][0], X_wor[1][0]], [O_wor[2][0], X_wor[2][0]], **arrow_prop_dict, color='r')
#ax1.add_artist(a)
#a = Arrow3D([O_wor[0][0], Y_wor[0][0]], [O_wor[1][0], Y_wor[1][0]], [O_wor[2][0], Y_wor[2][0]], **arrow_prop_dict, color='g')
#ax1.add_artist(a)
#a = Arrow3D([O_wor[0][0], Z_wor[0][0]], [O_wor[1][0], Z_wor[1][0]], [O_wor[2][0], Z_wor[2][0]], **arrow_prop_dict, color='b')
#ax1.add_artist(a)
#ax1.text(0, 0, 0.1, r'$W$')
#plt.show()
