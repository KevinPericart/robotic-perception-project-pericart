//#include <epuck.h>
#include <tp_epuck_hae806/control_functions.hpp>
#include <epuck/robot.hpp>

#include <opencv2/highgui.hpp>

#include <iostream>

constexpr std::string_view color_cout_reset = "\033[0m";

constexpr std::string_view color_cout_blue_bright = "\033[1;34m";

/*****************************/
/**** Main Program ***********/
/*****************************/

void IncorrectArguments(const int& argc);

int main(int argc, char** argv) {
    if (argc != 2) {
        IncorrectArguments(argc);
    }

    std::string name_of_read_folder = argv[1];
    int cnt = 1;
    Pose cur_pose_from_enc;
    Pose cur_pose_from_vis;
    Pose init_pose;
    std::vector<double> x_enc;
    std::vector<double> y_enc;
    std::vector<double> th_enc;
    x_enc = loadSensorLogs(name_of_read_folder + "data/xEnc.txt");
    y_enc = loadSensorLogs(name_of_read_folder + "data/yEnc.txt");
    th_enc = loadSensorLogs(name_of_read_folder + "data/thEnc.txt");

    auto distances = loadSensorLogs(name_of_read_folder + "data/distances.txt");
    if (distances.size() % 8 != 0) {
        std::cout << "Invalid number of IR columns\n";
        return 1;
    }

    Logger logger;
    cv::Mat col_img_from_disk;
    init_pose.setPose(.32, 0., M_PI);
    Robot robot{RobotType::V2};
    while (true) {
        std::cout << color_cout_blue_bright; // write in bold cyan
        std::cout << "\nSTART ITERATION " << cnt << " \n";
        std::cout << color_cout_reset; // reset color

        if (cnt == 1) {
            cur_pose_from_enc = init_pose;
            cur_pose_from_vis = init_pose;
            col_img_from_disk =
                loadAndShowImageFromDisk(name_of_read_folder, 2);
        } else {
            cur_pose_from_enc.x = x_enc[cnt - 1];
            cur_pose_from_enc.y = y_enc[cnt - 1];
            cur_pose_from_enc.th = th_enc[cnt - 1];
            col_img_from_disk =
                loadAndShowImageFromDisk(name_of_read_folder, cnt);
        }
        robot.current_pose = cur_pose_from_enc;
        double area_pix;
        cv::Point baryc =
            processImageToGetBarycenter(col_img_from_disk, area_pix);
        cur_pose_from_vis = getCurrPoseFromVision(baryc, cur_pose_from_enc.th,
                                                  area_pix, logger);
        double v,w;
        struct timeval startTime;
        controlRobotWithVisualServoing(baryc, area_pix,v,w);

        std::vector<cv::Point2f> prox_in_w_frame;
        double m_world;
        double p_world;
        double m_rob;
        double p_rob;
        bool line_found;
        for (int i = 0; i < 8; i++) {
            robot.proximity_sensors.ir_calibrated[i] =
                distances[(cnt - 1) * 8 + i];
        }
        convertIRPointsForWallFollowing(robot, prox_in_w_frame, m_rob, p_rob,
                                        line_found, m_world, p_world);
        drawMapWithRobot(robot, cur_pose_from_enc, cur_pose_from_vis,
                         prox_in_w_frame, line_found, m_world, p_world);

        cv::waitKey(0);
        cnt++;
    }
    return 0;
}

void IncorrectArguments(const int& argc) {
    printf("There are %d arguments instead of 1\n", argc - 1);
    printf("The argument should be the path to the folder containing the "
           "images to be processed \n");
    exit(0);
}
