#include <tp_epuck_hae806/main_functions.hpp>

#include <tp_epuck_hae806/control_functions.hpp>

#include <opencv2/highgui.hpp>

#include <iostream>

constexpr bool debug_infrared = false;
constexpr bool debug_odometry = false;

constexpr double wheel_diameter = 0.04;

// Distance between wheels in meters (axis length).
constexpr double wheel_distance = 0.053;

// Wheel circumference (meters).
constexpr double wheel_circumference = wheel_diameter * M_PI;

// Distance for each motor step (meters); a complete turn is 1000
// steps (0.000125 meters per step (m/steps)).
constexpr double mot_step_dist = (wheel_circumference / 1000.0 * 2 * M_PI);

void parseArguments(Robot& robot, int argc, char** argv) {
    std::string cont = argv[1];

    if (cont == "setWheelCmd") {
        if (argc != 4) {
            incorrectArguments(argc);
        } else {
            robot.controller = Controller::SetWheelCmd;
            robot.wheels_command.left_velocity = std::atof(argv[2]);
            robot.wheels_command.right_velocity = std::atof(argv[3]);
        }
    } else if (cont == "setVel") {
        if (argc != 4) {
            incorrectArguments(argc);
        } else {
            robot.controller = Controller::SetVel;
            robot.lin_velocity = std::stod(argv[2]);
            robot.ang_velocity = std::stod(argv[3]);
        }
    } else if (cont == "visServo") {
        if (argc != 2) {
            incorrectArguments(argc);
        } else {
            robot.controller = Controller::VisServo;
        }
    } else if (cont == "followWall") {
        if (argc != 2) {
            incorrectArguments(argc);
        } else {
            robot.controller = Controller::FollowWall;
        }
    } else {
        incorrectArguments(argc);
    }
}

void incorrectArguments(const int& argc) {
    std::cout << "There are " << argc - 1
              << " arguments, which is incorrect.\n";
    std::cout << "The first argument should be one of the "
                 "following:"
                 "\n\tsetWheelCmd\n\tsetRobVel\n\tfollowWall\n\tvisServo\n";
    std::cout << "The following arguments should be:\n";
    std::cout << "\tsetWheelCmd: leftWheelCmd rightWheelCmd\n";
    std::cout << "\tsetVel: v(linear vel) w(angular vel)\n";
    std::cout << "\tvisServo\n";
    std::cout << "\tfollowWall\n";
    exit(0);
}

void showAndSaveRobotImage(const Robot& robot, Logger& logger,
                           size_t iteration) {
    // show the image
    cv::namedWindow("Robot image",
                    cv::WINDOW_AUTOSIZE); // Create a window for display.
    cv::moveWindow("Robot image", 0, 0);  // Move window
    cv::imshow("Robot image",
               robot.camera_image); // Show the image inside it.
    cv::waitKey(1);
    // save the image on disk
    cv::imwrite(logger.folder_ + "/image/image" +
                    std::string(4 - std::to_string(iteration).length(), '0')
                        .append(std::to_string(iteration)) +
                    ".png",
                robot.camera_image);
}

void saveProximitySensors(const Robot& robot, Logger& logger) {
    auto log = [&logger](const double* data, size_t count) {
        for (size_t i = 0; i < count - 1; i++) {
            logger.addIn(logger.file_distances, data[i], "\t");
        }
        logger.addIn(logger.file_distances, data[count - 1]);
    };

    log(robot.proximity_sensors.ir_calibrated.data(),
        robot.proximity_sensors.ir_calibrated.size());
}

void savePosition(const Robot& robot, Logger& logger) {
    logger.addIn(logger.file_epuck_pose[0], robot.current_pose.x);
    logger.addIn(logger.file_epuck_pose[1], robot.current_pose.y);
    logger.addIn(logger.file_epuck_pose[2], robot.current_pose.th);

    logger.addIn(logger.file_epuck_left_wheel_position,
                 robot.wheels_state.left_position);
    logger.addIn(logger.file_epuck_right_wheel_position,
                 robot.wheels_state.right_position);

    logger.addIn(logger.file_epuck_left_wheel_velocity,
                 robot.wheels_state.left_velocity);
    logger.addIn(logger.file_epuck_right_wheel_velocity,
                 robot.wheels_state.right_velocity);
}

void printSensors(const Robot& robot,
                  std::chrono::duration<double> time_since_start) {

    std::cout << "ePuck pose :  x : " << robot.current_pose.x
              << ", y : " << robot.current_pose.y
              << ", th : " << robot.current_pose.th << "\n"
              << "Joint position [rad] :  Left : "
              << robot.wheels_state.left_position
              << ", Right : " << robot.wheels_state.right_position << "\n"
              << "Speed [rad/s]   : Left : " << robot.wheels_state.left_velocity
              << ", Right : " << robot.wheels_state.right_velocity << "\n"
              << "TimeSinceStart  : " << time_since_start.count() << "s"
              << std::endl;

    if constexpr (debug_infrared) {
        if (robot.proximity_sensors.ir_calibrated[0] > 0) {
            std::cout << "IR0 : " << robot.proximity_sensors.ir_calibrated[0]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR0 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[1] > 0) {
            std::cout << "IR1 : " << robot.proximity_sensors.ir_calibrated[1]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR1 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[2] > 0) {
            std::cout << "IR2 : " << robot.proximity_sensors.ir_calibrated[2]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR2 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[3] > 0) {
            std::cout << "IR3 : " << robot.proximity_sensors.ir_calibrated[3]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR3 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[4] > 0) {
            std::cout << "IR4 : " << robot.proximity_sensors.ir_calibrated[4]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR4 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[5] > 0) {
            std::cout << "IR5 : " << robot.proximity_sensors.ir_calibrated[5]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR5 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[6] > 0) {
            std::cout << "IR6 : " << robot.proximity_sensors.ir_calibrated[6]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR6 : NONE" << color_cout_reset
                      << "\n";
        }
        if (robot.proximity_sensors.ir_calibrated[7] > 0) {
            std::cout << "IR7 : " << robot.proximity_sensors.ir_calibrated[7]
                      << "\n";
        } else {
            std::cout << color_cout_black << "IR7 : NONE" << color_cout_reset
                      << "\n";
        }
    }
}

void proxDataRawValuesToMeters(Robot& robot) {
    for (int i = 0; i < 8; i++) {
        // Transform the analog value to a distance value in meters (given from
        // field tests).
        if (robot.proximity_sensors.ir_calibrated[i] > 0) {
            robot.proximity_sensors.ir_calibrated[i] =
                (0.5 / sqrt(robot.proximity_sensors.ir_calibrated[i]));
        } else {
            // Sometimes the values could be negative due to the calibration, it
            // means there is no obstacles.
            robot.proximity_sensors.ir_calibrated[i] = 1;
        }
    }
}

void odometryAndWheelsSpeed(Robot& robot, OdometryCorrectionData& data,
                            size_t iteration,
                            std::chrono::duration<double> iteration_delta_time) {

    if constexpr (debug_odometry) {
        std::cout << "left, right raw: " << robot.wheels_state.left_encoder
                  << ", " << robot.wheels_state.right_encoder << std::endl;
    }
    if constexpr (debug_odometry) {
        std::cout << "left, right raw corrected: "
                  << robot.wheels_state.left_encoder_corrected << ", "
                  << robot.wheels_state.right_encoder_corrected << std::endl;
    }

    // Compute odometry.
    data.left_steps_diff =
        double(robot.wheels_state.left_encoder_corrected) * mot_step_dist -
        data.left_steps_prev; // Expressed in meters.

    data.right_steps_diff =
        double(robot.wheels_state.right_encoder_corrected) * mot_step_dist -
        data.right_steps_prev; // Expressed in meters.

    if constexpr (debug_odometry) {
        std::cout << "left, right steps diff: " << data.left_steps_diff << ", "
                  << data.right_steps_diff << std::endl;
    }

    data.delta_theta = (data.right_steps_diff - data.left_steps_diff) /
                       wheel_distance; // Expressed in radiant.
    data.delta_steps = (data.right_steps_diff + data.left_steps_diff) /
                       2; // Expressed in meters.
    if constexpr (debug_odometry) {
        std::cout << "delta_theta, steps: " << data.delta_theta << ", "
                  << data.delta_steps << std::endl;
    }

    if (iteration > 1) {
        data.pose.x += data.delta_steps *
                       std::cos(data.pose.th +
                                data.delta_theta / 2); // Expressed in meters.
        data.pose.y += data.delta_steps *
                       std::sin(data.pose.th +
                                data.delta_theta / 2); // Expressed in meters.
        data.pose.th += data.delta_theta;              // Expressed in radiant.
    }

    if constexpr (debug_odometry) {
        std::cout << "x, y, theta: " << data.pose.x << ", " << data.pose.y
                  << ", " << data.pose.th << std::endl;
    }

    data.left_steps_prev = double(robot.wheels_state.left_encoder_corrected) *
                           mot_step_dist; // Expressed in meters.
    data.right_steps_prev = double(robot.wheels_state.right_encoder_corrected) *
                            mot_step_dist; // Expressed in meters.

    robot.current_pose.x = data.pose.x;
    robot.current_pose.y = data.pose.y;
    robot.current_pose.th = data.pose.th;

    data.time_ten_iter += iteration_delta_time.count();
    data.left_steps_diff_avg += data.left_steps_diff;
    data.right_steps_diff_avg += data.right_steps_diff;

    if (iteration % 10 == 0) {
        robot.wheels_state.left_velocity =
            (data.left_steps_diff_avg / data.time_ten_iter) /
            robot.parameters.wheel_radius;
        robot.wheels_state.right_velocity =
            (data.right_steps_diff_avg / data.time_ten_iter) /
            robot.parameters.wheel_radius;
        data.left_steps_diff_avg = 0;
        data.right_steps_diff_avg = 0;
        data.time_ten_iter = 0;
    }
}
