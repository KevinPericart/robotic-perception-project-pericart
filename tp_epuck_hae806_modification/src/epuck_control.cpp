#include <array>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <sensor_msgs/msg/range.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/highgui.hpp>
#include <fmt/format.h>

#include <epuck/robot.hpp>
#include <epuck/logger.hpp>

#include <tp_epuck_hae806/control_functions.hpp>
#include <tp_epuck_hae806/main_functions.hpp>

#include <atomic>
#include <chrono>
#include <csignal>
#include <iostream>
#include <string>
#include <utility>

using sensor_msgs::msg::Image;
using sensor_msgs::msg::JointState;
using sensor_msgs::msg::Range;
using std_msgs::msg::Float64MultiArray;

class EPuckController : public rclcpp::Node {
public:
    EPuckController(Robot robot)
        : Node("epuck_controller"), robot_{std::move(robot)} {
        cmd_publisher_ = create_publisher<Float64MultiArray>(
            "/wheels_velocity_controller/commands", 10);

        joint_state_subscription_ = create_subscription<JointState>(
            "/joint_states", 10,
            [this](JointState::SharedPtr msg) { joint_state_callback(msg); });

        for (size_t i = 0; i < 8; i++) {
            laser_subscriptions_[i] = create_subscription<Range>(
                fmt::format("/ps{}/out", i), 10,
                [this, i](Range::SharedPtr msg) { laser_callback(msg, i); });
        }

        camera_subscription_ = create_subscription<Image>(
            "/camera/image_raw", 10,
            [this](Image::SharedPtr msg) { camera_callback(msg); });

        cmd_.data.resize(2, 0);

        last_iteration_ = std::chrono::steady_clock::now();

        using namespace std::chrono_literals;
        run_timer_ = create_wall_timer(100ms, [this] { run(); });
    }

    void stop() {
        cmd_.data[0] = 0;
        cmd_.data[1] = 0;
        cmd_publisher_->publish(cmd_);
    }

    void run() {
        if (not(joint_state_received and camera_image_received and
                laser_received)) {
            return;
        }

        const auto now = std::chrono::steady_clock::now();
        iteration_delta_time_ = now - last_iteration_;
        last_iteration_ = now;

        time_since_start_ += iteration_delta_time_;

        if (iteration_ == 1) {
            odometry_correction_data_.pose = init_pose_;
            cur_pose_from_vis_ = init_pose_;
        }

        odometryAndWheelsSpeed(robot_, odometry_correction_data_, iteration_,
                               iteration_delta_time_);

        cur_pose_from_enc_ = robot_.current_pose;

        double area_pix;
        cv::Point baryc;
        baryc = processImageToGetBarycenter(robot_.camera_image, area_pix);

        
        cur_pose_from_vis_ = getCurrPoseFromVision(baryc, cur_pose_from_enc_.th,
                                                   area_pix, logger_);
        prev_pose_from_enc_ = cur_pose_from_enc_;
        prev_pose_from_vis_ = cur_pose_from_vis_;

        std::vector<cv::Point2f> prox_in_w_frame;
        double m_rob;
        double p_rob;
        double m_world;
        double p_world;
        bool line_found;

        convertIRPointsForWallFollowing(robot_, prox_in_w_frame, m_rob, p_rob,
                                        line_found, m_world, p_world);

        drawMapWithRobot(robot_, cur_pose_from_enc_, cur_pose_from_vis_,
                         prox_in_w_frame, line_found, m_world, p_world);

        switch (robot_.controller) {
        case Controller::SetWheelCmd:
            break;
        case Controller::SetVel:
            setRobotVelocities(robot_, robot_.lin_velocity, robot_.ang_velocity);
            break;
        case Controller::VisServo: {
            double vel;
            double omega;
            controlRobotWithVisualServoing(baryc, area_pix, vel, omega);
            setRobotVelocities(robot_, vel, omega);
            break;
        }
        case Controller::FollowWall: {
            double vel;
            double omega;
            controlRobotToFollowWall(line_found, m_rob, p_rob, vel, omega);
            setRobotVelocities(robot_, vel, omega);
            break;
        }
        }

        printSensors(robot_, time_since_start_);
        showAndSaveRobotImage(robot_, logger_, iteration_);
        saveProximitySensors(robot_, logger_);
        savePosition(robot_, logger_);

        ++iteration_;

        cmd_.data[0] = robot_.wheels_command.left_velocity;
        cmd_.data[1] = robot_.wheels_command.right_velocity;
        cmd_publisher_->publish(cmd_);
    }

private:
    void joint_state_callback(const JointState::SharedPtr& msg) {
        robot_.wheels_state.left_position = msg->position[0];
        robot_.wheels_state.right_position = msg->position[1];
        robot_.wheels_state.left_encoder_corrected =
            long(robot_.wheels_state.left_position *
                 double(robot_.parameters.steps_per_rev));
        robot_.wheels_state.right_encoder_corrected =
            long(robot_.wheels_state.right_position *
                 double(robot_.parameters.steps_per_rev));
        robot_.wheels_state.left_encoder =
            int16_t(robot_.wheels_state.left_encoder_corrected % 32767);
        robot_.wheels_state.right_encoder =
            int16_t(robot_.wheels_state.right_encoder_corrected % 32767);
        joint_state_received = true;
    }

    void camera_callback(const Image::SharedPtr& msg) {
        cv_image_ = cv_bridge::toCvCopy(msg);
        robot_.camera_image = cv_image_->image;
        camera_image_received = true;
    }

    void laser_callback(const Range::SharedPtr& msg, size_t index) {
        robot_.proximity_sensors.ir_calibrated[index] = msg->range;
        laser_received = true;
    }

    rclcpp::Publisher<Float64MultiArray>::SharedPtr cmd_publisher_;
    rclcpp::Subscription<JointState>::SharedPtr joint_state_subscription_;
    rclcpp::Subscription<Image>::SharedPtr camera_subscription_;
    std::array<rclcpp::Subscription<Range>::SharedPtr, 8> laser_subscriptions_;
    rclcpp::TimerBase::SharedPtr run_timer_;

    Float64MultiArray cmd_;
    cv_bridge::CvImagePtr cv_image_;

    Robot robot_;
    Logger logger_;

    Pose prev_pose_from_enc_;
    Pose cur_pose_from_enc_;
    Pose prev_pose_from_vis_;
    Pose cur_pose_from_vis_;
    Pose init_pose_{.32, 0., M_PI};
    OdometryCorrectionData odometry_correction_data_;

    size_t iteration_{1};
    std::chrono::duration<double> time_since_start_{0.};
    std::chrono::duration<double> iteration_delta_time_;
    std::chrono::steady_clock::time_point last_iteration_;

    bool joint_state_received{false};
    bool camera_image_received{false};
    bool laser_received{false};
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);

    if (argc == 1) {
        incorrectArguments(argc);
    }

    Robot robot{RobotType::V2};
    parseArguments(robot, argc, argv);
    auto epuck_controller = std::make_shared<EPuckController>(std::move(robot));

    rclcpp::spin(epuck_controller);

    rclcpp::shutdown();

    return 0;
}
