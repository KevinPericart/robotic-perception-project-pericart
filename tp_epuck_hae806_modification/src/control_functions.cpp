#include <tp_epuck_hae806/control_functions.hpp>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <cmath>
#include <iostream>
#include <chrono>

/**** Convert IR distances to points x y in robot frame, estimate line parameters
 * y = mx +p in robot frame and then convert everything to world frame ****/
void convertIRPointsForWallFollowing(
    const Robot& robot, std::vector<cv::Point2f>& all_points_in_world_frame,
    double& m_rob, double& p_rob, bool& line_found, double& m_world,
    double& p_world) {
    cv::Point2f one_point_temporary;
    std::vector<cv::Point2f> all_points_in_rob_frame;
    const auto& dist = robot.proximity_sensors.ir_calibrated;
    for (int i = 4; i < 8; i++) {
        printf("\nthe angle of IR number %d is %f radians ", i,
               robot.parameters.theta[i]);
        if (dist[i] < robot.parameters.dist_max) {
            printf("\033[0;31mInfraRed number %d measured at %f meters \033[0m",
                   i, dist[i]);
            one_point_temporary.x = (robot.parameters.robot_radius + dist[i]) * cos(robot.parameters.theta[i]+robot.current_pose.th);
            one_point_temporary.y =  (robot.parameters.robot_radius + dist[i])  * sin(robot.parameters.theta[i]+robot.current_pose.th) ; //+robot.current_pose.th ou non  robot.parameters.robot_radius + 
            all_points_in_rob_frame.push_back(one_point_temporary);
        }
    }
    printf("there are %ld points on the side\n", all_points_in_rob_frame.size());
    if (all_points_in_rob_frame.size() >= 2) {
        double x1 = all_points_in_rob_frame[0].x;
        double y1 = all_points_in_rob_frame[0].y;
        double x2 = all_points_in_rob_frame[1].x;
        double y2 = all_points_in_rob_frame[1].y;
       double dx = x2 - x1;
       double dy = y2 - y1;
       double m_rob = dy / dx ;
       double p_rob = y1 - m_rob*x1 ;
        line_found = true;
        // Projection sur le repere monde
        for (size_t i = 0; i < all_points_in_rob_frame.size(); i++) {
            double x_rob = all_points_in_rob_frame[i].x;
            double y_rob = all_points_in_rob_frame[i].y;
            double x_world = robot.current_pose.x +x_rob * cos(robot.current_pose.th) - y_rob * sin(robot.current_pose.th); // convertir x du repere robot au repere monde  + robot.current_pose.x +
            double y_world =  robot.current_pose.y +x_rob * sin(robot.current_pose.th) -y_rob * cos(robot.current_pose.th); // pareil pour y 
            one_point_temporary.x = x_world ;
            one_point_temporary.y = y_world ;
            all_points_in_world_frame.push_back(one_point_temporary);
            printf("point %ld in world frame: (%f, %f)\n", i+1, x_world, y_world);
        }
       double x1w = all_points_in_world_frame[0].x;
       double y1w = all_points_in_world_frame[0].y;
       double x2w = all_points_in_world_frame[1].x;
       double y2w = all_points_in_world_frame[1].y;
       double dxw = x2w - x1w;
       double dyw = y2w - y1w;
     m_world= dyw / dxw ;
     p_world = y1w - (m_world*x1w ) ;


    } else {
        line_found = false;
    }
}
/**** Derive robot pose from encoders and save it along with robot linear and
 * angular velocities ****/
Pose getCurrPoseFromEncoders(const Robot& robot, const Pose& prev_rob_pose,
                             double prev_enc_l, double prev_enc_r, Logger& log) {
    const double encoder_factor = 0.0072; // 0.0072 for V1
    const double sample_time =
        0.038; // 0.1 for V1;//TODO this should also be read from logs

    double lin_vel;
    double ang_vel;
    double dx;
    double dy;
    double d_th;
    double d_enc_l = robot.wheels_command.left_encoder - prev_enc_l;
    double d_enc_r = robot.wheels_command.right_encoder - prev_enc_r;
    if ((fabs(d_enc_l) > 150) ||
        (fabs(d_enc_r) > 150)) { // fix measuring errors
        lin_vel = 0;
        ang_vel = 0;
    } else {
        if (d_enc_l < -M_PI / 2) {
            d_enc_l = d_enc_l + M_PI;
        } else if (d_enc_l > M_PI / 2) {
            d_enc_l = d_enc_l - M_PI;
        }
        if (d_enc_r < -M_PI / 2) {
            d_enc_r = d_enc_r + M_PI;
        } else if (d_enc_r > M_PI / 2) {
            d_enc_r = d_enc_r - M_PI;
        }
        lin_vel = robot.parameters.wheel_radius * encoder_factor *
                  (d_enc_r + d_enc_l) / 2;
        ang_vel = robot.parameters.wheel_radius * encoder_factor *
                  (d_enc_r - d_enc_l) / robot.parameters.axis_length;
    }
    d_th = ang_vel * sample_time;
    dx = lin_vel * cos(prev_rob_pose.th + d_th);
    dy = lin_vel * sin(prev_rob_pose.th + d_th);

    Pose curs_rob_pose(prev_rob_pose.x + dx, prev_rob_pose.y + dy,
                      prev_rob_pose.th + d_th);

    // log the data

    log.addIn(log.file_v, lin_vel); // Operationnal linear speed
    log.addIn(log.file_w, ang_vel);
    log.addIn(log.file_dx, dx / sample_time);
    log.addIn(log.file_dy, dy / sample_time);
    log.addIn(log.file_time,
              sample_time); // TODO this should also be read from logs

    return (curs_rob_pose);
}

/**** Derive robot pose from vision (from barycenter of target) and save it ****/
Pose getCurrPoseFromVision([[maybe_unused]] const cv::Point& baryctr,
                           [[maybe_unused]] double theta,
                           [[maybe_unused]] double areaPix, Logger& log) {
    Pose cur_rob_pose_vis;
    cur_rob_pose_vis.setPose(.32, 0., M_PI); // TODOM2

    const double uo = 76.30766120622603;
    const double vo = 48.88868785955732;
    const double alpha_u = 235.1879436704074;
    const double alpha_v = 230.61686990440336;

    const double Am = 0.000004;

    double cZb, cXb, cYb, rXb, rYb;
    cZb = sqrt((Am * alpha_u * alpha_v)/(areaPix));

    cXb = ((baryctr.x - uo ) * cZb ) / alpha_u ;
    cYb = ((baryctr.y - vo ) * cZb ) / alpha_v ;



    //CpR =  [[ 0.00112015]
    //[-0.20377144]
    //[ 0.15004025]]

    rXb = cXb + (- 0.00112015);
    rYb = cYb + (  0.20377144);

    cur_rob_pose_vis.x = sin(theta)* (-rXb) + cos(theta)* (-rYb);
    cur_rob_pose_vis.y = -cos(theta)* (-rXb) + sin(theta)* (-rYb);
    // log the data
    log.addIn(log.file_x_vision, cur_rob_pose_vis.x);
    log.addIn(log.file_y_vision, cur_rob_pose_vis.y);
    log.addIn(log.file_th_vision, cur_rob_pose_vis.th);

    std::cout << "curRobPose from vision is " << cur_rob_pose_vis.x << " y "
              << cur_rob_pose_vis.y << " th " << cur_rob_pose_vis.th
              << std::endl; // TODAY
    return (cur_rob_pose_vis);
}
/**** Show image from the robot camera and save it on the disk ****/
///////////// NOT USED IN EPUCK-APP //////////////////////
cv::Mat loadAndShowImageFromDisk(const std::string& folder_name,
                                 const int& cnt_it) {
    // load the image from the disk
    char img_name_iteration[30];
    sprintf(img_name_iteration, "image/image%04d.png", cnt_it);
    cv::Mat color_raw_img = cv::imread(folder_name + img_name_iteration);
    std::cout << "\nLoadImage loaded image " << folder_name + img_name_iteration
              << std::endl;
    cv::namedWindow("loggedImg",
                    cv::WINDOW_AUTOSIZE);   // Create a window for display
    cv::moveWindow("loggedImg", 0, 0);      // Move window
    cv::imshow("loggedImg", color_raw_img); // Show the image inside it
    return (color_raw_img);
}

/**** Load encoder measurements from logs on disk ****/ //////////// NOT USED IN
                                                        /// EPUCK-APP
                                                        /////////////////////////

std::vector<double> loadSensorLogs(const std::string& folder_name) {
    std::vector<double> sens_data;
    // load the data from the disk
    FILE* sen;
    sen = fopen(folder_name.c_str(), "r");
    std::cout << "Loading sensor from " << folder_name << std::endl;
    int i = 0;
    double tmp;
    while (fscanf(sen, "%lf", &tmp) != EOF) {
        sens_data.push_back(tmp);
        //        std::cout << "i " << i <<"\t sen "<< sensData[i] <<"\n";
        i++;
    };
    int size = i;
    std::cout << "size " << size << std::endl;
    return (sens_data);
}

void drawMapWithRobot(const Robot& robot, const Pose& r_pose_enc,
                      const Pose& r_pose_vis,
                      std::vector<cv::Point2f> prox_in_w_fr,
                      const bool& line_found, double m_wall, double p_wall) {
    cv::Mat map_img;
    static const int cm_in_pixels = 15;
    static const int x_max_in_cm = 60;
    static const int y_max_in_cm = 40;
    static const int x_origin_in_cm = 5;
    // draw empty map
    map_img.create((x_max_in_cm + x_origin_in_cm) * cm_in_pixels,
                   y_max_in_cm * cm_in_pixels, CV_8UC3);
    map_img.setTo(cv::Scalar(255, 255, 255));

    cv::Point origin((y_max_in_cm / 2) * cm_in_pixels,
                     x_origin_in_cm * cm_in_pixels);
    // draw grid
    // draw horizontal
    for (int i = 0; i <= 32; i++) {
        cv::line(map_img,
                 origin + cv::Point(-11 * cm_in_pixels, i * cm_in_pixels),
                 origin + cv::Point(11 * cm_in_pixels, i * cm_in_pixels),
                 cv::Scalar(200, 200, 200), 1);
    }
    // draw vertical
    for (int i = 0; i <= 22; i++) {
        cv::line(map_img,
                 origin + cv::Point(-11 * cm_in_pixels + i * cm_in_pixels, 0),
                 origin + cv::Point(-11 * cm_in_pixels + i * cm_in_pixels,
                                    32 * cm_in_pixels),
                 cv::Scalar(200, 200, 200), 1);
    }

    // draw scale
    cv::line(
        map_img,
        cv::Point(x_origin_in_cm * cm_in_pixels, x_origin_in_cm * cm_in_pixels),
        cv::Point(x_origin_in_cm * cm_in_pixels,
                  (x_origin_in_cm + 1) * cm_in_pixels),
        cv::Scalar(0, 100, 0), 2);
    std::string cm_text = "1 cm";
    cv::putText(map_img, cm_text,
                cv::Point(x_origin_in_cm * cm_in_pixels + 3,
                          (x_origin_in_cm + 0.5) * cm_in_pixels),
                cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 100, 0), 2);

    // draw x axis
    cv::arrowedLine(map_img, origin,
                    origin + cv::Point(0, x_max_in_cm / 3 * cm_in_pixels),
                    cv::Scalar(255, 0, 0), 2, 8, 0, 0.03);
    std::string x_text = "X";
    cv::putText(map_img, x_text, origin + cv::Point(-cm_in_pixels, cm_in_pixels),
                cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255, 0, 0), 2);

    // draw y axis
    cv::arrowedLine(map_img, origin,
                    origin + cv::Point(y_max_in_cm / 3 * cm_in_pixels, 0),
                    cv::Scalar(255, 0, 0), 2, 8, 0, 0.03);
    std::string y_text = "Y";
    cv::putText(map_img, y_text, origin + cv::Point(cm_in_pixels, -cm_in_pixels),
                cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(255, 0, 0), 2);

    // draw robot pose from encoders
    double th_enc = r_pose_enc.th;
    int u_enc = (int(r_pose_enc.y * 100) + y_max_in_cm / 2) * cm_in_pixels;
    int v_enc = (int(r_pose_enc.x * 100) + x_origin_in_cm) * cm_in_pixels;
    double rob_radius = robot.parameters.robot_radius * 100 * cm_in_pixels;
    cv::line(map_img, cv::Point(u_enc, v_enc),
             cv::Point(u_enc - int(rob_radius * sin(th_enc - M_PI)),
                       v_enc - int(rob_radius * cos(th_enc - M_PI))),
             cv::Scalar(0, 0, 255), 2);
    cv::circle(map_img, cv::Point(u_enc, v_enc), int(rob_radius),
               cv::Scalar(0, 0, 255), 2);

    // draw IR in black
    for (auto& point : prox_in_w_fr) {
        cv::Point i_rin_map;
        // i_rin_map.x = (int(point.y * 100) + x_max_in_cm / 2) * cm_in_pixels;
        i_rin_map.x = (int(point.y * 100) + y_max_in_cm / 2) * cm_in_pixels;
        i_rin_map.y = (int(point.x * 100) + x_origin_in_cm) * cm_in_pixels;
        cv::line(map_img, cv::Point(u_enc, v_enc), i_rin_map,
                 cv::Scalar(0, 0, 0), 3);
    }
    // draw wall in green
    if (line_found) {
        cv::Point pt1in_map;
        cv::Point pt2in_map;
        pt1in_map.x =
            (int((-m_wall + p_wall) * 100) + y_max_in_cm / 2) * cm_in_pixels;
        pt1in_map.y = (-100 + x_origin_in_cm) * cm_in_pixels;
        pt2in_map.x =
            (int((m_wall + p_wall) * 100) + y_max_in_cm / 2) * cm_in_pixels;
        pt2in_map.y = (100 + x_origin_in_cm) * cm_in_pixels;
        cv::line(map_img, pt1in_map, pt2in_map, cv::Scalar(0, 255, 0), 2);
    }

    // draw robot pose from vision
    double x_vis = r_pose_vis.x;
    double y_vis = r_pose_vis.y;
    double th_vis = r_pose_vis.th;
    int u_vis = (int(y_vis * 100) + y_max_in_cm / 2) * cm_in_pixels;
    int v_vis = (int(x_vis * 100) + x_origin_in_cm) * cm_in_pixels;
    cv::line(map_img, cv::Point(u_vis, v_vis),
             cv::Point(u_vis - int(rob_radius * sin(th_vis - M_PI)),
                       v_vis - int(rob_radius * cos(th_vis - M_PI))),
             cv::Scalar(0, 0, 0), 1);
    cv::circle(map_img, cv::Point(u_vis, v_vis), int(rob_radius),
               cv::Scalar(0, 0, 0), 1);

    // show map
    cv::namedWindow("map", cv::WINDOW_AUTOSIZE); // Create a window for display
    cv::moveWindow("map", 1020, 0);              // Move window
    cv::imshow("map", map_img);                  // Show the image inside it
}

cv::Point processImageToGetBarycenter(cv::Mat& col_raw_img, double& area_pix) {
    // get start time of function
    const auto start = std::chrono::steady_clock::now();

    cv::Mat grey_img;
    cv::cvtColor(col_raw_img, grey_img,
                 cv::COLOR_BGR2GRAY); // convert image to black and white

    // process images to get barycenter
    cv::Mat processed_img;
    // TODOM2 below are the default coordinates - to be changed
    cv::Mat thr;

    
    // convert grayscale to binary image
    cv::threshold( grey_img, thr, 150,255,cv::THRESH_BINARY_INV );
 
    // find moments of the image
    cv::Moments m = cv::moments(thr,true);
    cv::Point p(m.m10/m.m00, m.m01/m.m00);



    cv::cvtColor(grey_img, processed_img,
                 cv::COLOR_GRAY2BGR); // convert image to color (for displaying
                                      // colored features)
    cv::Point target_barycenter(p);
    area_pix = m.m00;

    std::cout << "targetBarycenter is at " << p << " area is "
              << area_pix << std::endl; // print coordinates
    cv::circle(processed_img, target_barycenter, 3, cv::Scalar(0, 0, 255),
               1); // display red circle
    cv::circle(processed_img, cv::Point(80, 100), 3, cv::Scalar(255, 0, 0),
               1); // display red circle
    cv::circle(grey_img, target_barycenter, 3, cv::Scalar(255, 255, 255),
               1); // display white circle

    // show the images
    cv::namedWindow("greyImg",
                    cv::WINDOW_AUTOSIZE); // Create a window for display
    cv::moveWindow("greyImg", 340, 0);    // Move window
    cv::imshow("greyImg", grey_img);      // Show the image inside it
    cv::namedWindow("processedImg",
                    cv::WINDOW_AUTOSIZE);      // Create a window for display
    cv::moveWindow("processedImg", 680, 0);    // Move window
    cv::imshow("processedImg", processed_img); // Show the image inside it

    const auto end = std::chrono::steady_clock::now();
    const auto time_spent =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    std::cout << "processImageToGetBarycenter took " << time_spent.count()
              << " ms\n"; // print time spent for processing //TODAY
    return (target_barycenter);
}

/**** Send commands (speed_L, speed_R) to motors in order to realize robot
 * velocities (v, w) for 10 seconds ****/
void setRobotVelocities(Robot& robot, double vel, double omega) {
    std::cout << "The required robot velocities are v " << vel << " w " << omega
              << "\n\n\n";

    // Commands to be sent to the left and right wheels
    //  robVelLeft && robVelRight ??
    // int speedL;
    // int speedR;
    const double enc_factor = 0.012;

    robot.wheels_command.left_velocity =
        (2 * vel - robot.parameters.axis_length * omega) /
        (robot.parameters.wheel_radius *
         enc_factor); // TODO there is a 2 * error factor on pure rotation
                      // (robot pivots twice faster than required)
    robot.wheels_command.right_velocity =
        (2 * vel + robot.parameters.axis_length * omega) /
        (robot.parameters.wheel_radius *
         enc_factor);

    std::cout << "SetRobotVelocities wheel speeds are "
              << robot.wheels_command.left_velocity << " "
              << robot.wheels_command.right_velocity << "\n";
}


void controlRobotWithVisualServoing(const cv::Point& baryc, double area_pix, double& vel,
                                    double& omega) {
    // Parameters for the control law (you may need to adjust these)



    const double Kp_linear = 0.01;
    const double Kp_angular = 0.00005;

    // Target image coordinates (center of the image)
    const double uo = 76.30766120622603;
    const double vo = 48.88868785955732;
    const double alpha_u = 235.1879436704074;
    const double alpha_v = 230.61686990440336;

    const double Am = 0.000004;

    double cZb;
    cZb = sqrt((Am * alpha_u * alpha_v)/(area_pix));

    // Error in image coordinates
    const double error_u = baryc.x - alpha_u;
    const double error_v = uo - baryc.x ;

    // Control law for linear velocity (proportional to the image error)
    vel = (Kp_linear * cZb);

    // Control law for angular velocity (proportional to the image error)
    omega = Kp_angular * error_v;


    // Print debug information
    //printf("Control law: vel=%f, omega=%f, C_Z_B=%f\n", vel, omega, C_Z_B);
    printf("Control law: vel=%f, omega=%f\n", vel, omega);
}




// make robot follow a wall using infrared measurements for ten seconds
void controlRobotToFollowWall(bool line_found, double m_rob, double p_rob,
                              double& vel, double& omega) {
                              
    // set constant dStar
    const double Kd = -0.03;  //-0.03
    const double Kalpha = 0.15;  //0.03
    double d = (-p_rob) / sqrt(1 + m_rob*m_rob);
    double alpha = atan(m_rob);  //+ d *3.14/2
    [[maybe_unused]] const double d_star = 0.03;
    if (line_found) {
        std::cout << "equation of the line in the robot frame: y = " << m_rob
                  << " x + " << p_rob << " \n";
               
              
        // from mRob and pRob calculate d and alpha
        const double Kd = -0.05;  //-0.03
        const double Kalpha = 0.15;  //0.03
        double d = (-p_rob) / sqrt(1 + m_rob*m_rob);
        double alpha = atan(m_rob);  //+ d *3.14/2
        printf("alpha = %d \n",alpha);
        //double d = p_rob * cos(alpha);
        vel = 0.0002;
        if ( alpha>0){
        omega = (Kd * (d - d_star) + Kalpha*alpha);
        }
        else {
        omega = Kd * (d - d_star) + Kalpha*alpha;
        }
        
    } else {
        vel = 0.0002;
        //omega = 0;
        omega = -(Kd * (d - d_star) + Kalpha*alpha);
    }
    
    std::cout << "robot velocities are vel = " << vel << " omega " << omega
              << " \n";
}
