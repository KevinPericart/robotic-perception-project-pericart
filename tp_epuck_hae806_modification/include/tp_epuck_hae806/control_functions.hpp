#pragma once

/* CONTROL FUNCTIONS FOR EPUCK ROBOT*/

#include <epuck/logger.hpp>
#include <epuck/robot.hpp>

#include <string>
#include <vector>

/**** Derive robot pose from encoders and save it along with robot linear and
 * angular velocities ****/
Pose getCurrPoseFromEncoders(const Robot& robot, const Pose& prev_rob_pose,
                             double prev_enc_l, double prev_enc_r, Logger& log);

/**** Derive robot pose from vision (from barycenter of target) and save it ****/
Pose getCurrPoseFromVision(const cv::Point& baryctr, double theta,
                           double area_pix, Logger& log);

/**** Load image from the disk ****/ //////////// NOT USED IN EPUCK-APP
                                     /////////////////////////
cv::Mat loadAndShowImageFromDisk(const std::string& folder_name,
                                 const int& cnt_it);

/**** Load encoder measurements from logs on disk ****/ //////////// NOT USED IN
                                                        /// EPUCK-APP
                                                        /////////////////////////
std::vector<double> loadSensorLogs(const std::string& folder_name);

/**** Convert IR distances to points x y in robot frame, estimate line parameters
 * y = mx +p in robot frame and then convert everything to world frame ****/
void convertIRPointsForWallFollowing(
    const Robot& robot, std::vector<cv::Point2f>& all_points_in_world_frame,
    double& m_rob, double& p_rob, bool& line_found, double& m_world,
    double& p_world);

/**** Draws a map with world frame and robot ****/
void drawMapWithRobot(const Robot& robot, const Pose& r_pose_enc,
                      const Pose& r_pose_vis,
                      std::vector<cv::Point2f> prox_in_w_fr,
                      const bool& line_found, double m_wall, double p_wall);

/**** Process image compute and draw barycenter ****/
cv::Point processImageToGetBarycenter(cv::Mat& col_raw_img, double& area_pix);

/**** Send commands (speed_L, speed_R) to motors in order to realize robot
 * velocities (v, w) for 10 seconds ****/
void setRobotVelocities(Robot& robot, double vel, double omega);

// control robot using images from the camera
void controlRobotWithVisualServoing(const cv::Point& baryc, double area_pix, double& vel,
                                    double& omega);

// make robot follow a wall using infrared measurements for ten seconds
void controlRobotToFollowWall(bool line_found, double m_rob, double p_rob,
                              double& vel, double& omega);
